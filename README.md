## Leagues App

## Overview
This application show teams for searched input league.

It contain one principal screen which allows users to make search for teams belonging to the entered league name.

The search field offers suggestions to facilitate users inputs.

There is a caching mechanism with API and Coil HTTPS requests to avoid wasting resources.

## Technical stack :
# - Language : Kotlin
# - UI : Jetpack Compose
# - Architecture : MVVM and clean
# - Dependency injection : Dagger-Hilt
# - Build configuration language : Kotlin DSL
# - Network & API : Retrofit, OkHttp, Moshi
# - API Rest : [thesportsdb](https://www.thesportsdb.com/api.php)
# - Image Loading : [Coil Compose](https://coil-kt.github.io/coil/compose)
# - Code inspect and format : [JLLeitschuh Ktlint](https://github.com/JLLeitschuh/ktlint-gradle)
# - Unit testing : Jupiter + Mockk

|                                                                   |                                                                 |                                                                             |
|:-----------------------------------------------------------------:|:---------------------------------------------------------------:|:---------------------------------------------------------------------------:|
|  <img width="1604" alt="search_dark" src="docs/search_dark.png">  |  <img width="1604" alt="teams_dark" src="docs/teams_dark.png">  |  <img width="1604" alt="teams_error_dark" src="docs/teams_error_dark.png">  |
| <img width="1604" alt="search_light" src="docs/search_light.png"> | <img width="1604" alt="teams_light" src="docs/teams_light.png"> | <img width="1604" alt="teams_error_light" src="docs/teams_error_light.png"> |

