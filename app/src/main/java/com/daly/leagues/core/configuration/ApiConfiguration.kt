package com.daly.leagues.core.configuration

object ApiConfiguration {

    // Get API key with obfuscated function
    fun KVwukSRVOw(): String {
        val obfuscated = intArrayOf(0x35, 0x1e, 0x0c, 0x06, 0x1f, 0x0c, 0x31, 0xae)
        val result = ByteArray(8)
        result[0] = obfuscated[0].xor(0).toByte()
        result[1] = obfuscated[3].shl(3).plus(0).toByte()
        result[2] = obfuscated[1].plus(19).toByte()
        result[3] = obfuscated[7].minus(123).toByte()
        result[4] = obfuscated[2].shl(2).plus(0).toByte()
        result[5] = obfuscated[6].xor(0).toByte()
        result[6] = obfuscated[4].plus(23).toByte()
        result[7] = obfuscated[5].shl(2).plus(2).toByte()
        return result.toString(Charsets.UTF_8)
    }
}
