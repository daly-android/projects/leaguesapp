package com.daly.leagues.core.implementations

import com.daly.leagues.core.DispatcherService
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

class DispatcherServiceImpl : DispatcherService {
    override val io: CoroutineContext get() = Dispatchers.IO
    override val main: CoroutineContext get() = Dispatchers.Main
    override val default: CoroutineContext get() = Dispatchers.Default
    override val unconfined: CoroutineContext get() = Dispatchers.Unconfined
}
