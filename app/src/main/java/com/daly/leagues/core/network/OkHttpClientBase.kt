package com.daly.leagues.core.network

import okhttp3.OkHttpClient
import okhttp3.Protocol
import timber.log.Timber
import java.util.concurrent.TimeUnit

object OkHttpClientBase {

    private const val TIMEOUT = 30L

    /**
     * Provide okhttp client builder
     * @return okhttp client builder with timeout
     */
    fun buildOkHttpClient(httpCache: HttpCache): OkHttpClient {
        Timber.d("OkHttpClientBase.buildOkHttpClient")
        val builder = OkHttpClient.Builder()
            .cache(httpCache.buildHttpCache())
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .protocols(listOf(Protocol.HTTP_1_1))
            .followRedirects(false)
            .followSslRedirects(false)
            .addInterceptor(OkHttpInterceptors.buildHttpLoggingInterceptor()) // Add logging interceptor
            .addNetworkInterceptor(OkHttpInterceptors.buildNetworkCacheInterceptor()) // Add cache interceptor

        return builder.build()
    }
}
