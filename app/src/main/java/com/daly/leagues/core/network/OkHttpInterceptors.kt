package com.daly.leagues.core.network

import com.daly.leagues.BuildConfig
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

object OkHttpInterceptors {

    private const val CACHE_AGE_DAYS = 5
    private const val HEADER_CACHE_CONTROL = "Cache-Control"

    /**
     * Build an [HttpLoggingInterceptor] and define its level according to the [BuildConfig] Debug or Release
     * @return [HttpLoggingInterceptor]
     */
    fun buildHttpLoggingInterceptor() = HttpLoggingInterceptor(logger = OkHttpLogger()).apply {
        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    }

    /**
     * Cache only successful responses for 5 days
     */
    fun buildNetworkCacheInterceptor() = Interceptor { chain ->
        var response: Response = chain.proceed(chain.request())

        if (response.isSuccessful) {
            val cacheControl: CacheControl = CacheControl.Builder()
                .maxAge(CACHE_AGE_DAYS, TimeUnit.DAYS) // cache duration
                .build()
            response = response.newBuilder()
                .header(HEADER_CACHE_CONTROL, cacheControl.toString())
                .build()
        }

        return@Interceptor response
    }
}
