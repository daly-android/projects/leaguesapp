package com.daly.leagues.data.mappers

import com.daly.leagues.data.remote.dto.LeagueDto
import com.daly.leagues.domain.models.LeagueModel

fun List<LeagueDto>.toDomainModel() = this.mapNotNull { it.toDomainModel() }

private fun LeagueDto.toDomainModel() = strLeague?.let { LeagueModel(name = it) }
