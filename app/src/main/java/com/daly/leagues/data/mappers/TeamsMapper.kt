package com.daly.leagues.data.mappers

import com.daly.leagues.data.remote.dto.TeamDto
import com.daly.leagues.domain.models.TeamModel

fun List<TeamDto>.toDomainModel() = this.mapNotNull { it.toDomainModel() }

private fun TeamDto.toDomainModel(): TeamModel? {
    val name = strTeam ?: return null
    return TeamModel(name = name, badgeUrl = strTeamBadge)
}
