package com.daly.leagues.data.remote.api

import com.daly.leagues.core.configuration.ApiConfiguration
import com.daly.leagues.data.remote.dto.LeaguesDto
import com.daly.leagues.data.remote.dto.TeamsDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SportsDbApi {

    @GET("/api/v1/json/{apiKey}/all_leagues.php")
    suspend fun getLeagues(
        @Path("apiKey") apiKey: String = ApiConfiguration.KVwukSRVOw()
    ): Response<LeaguesDto>

    @GET("/api/v1/json/{apiKey}/search_all_teams.php")
    suspend fun getTeamsByLeague(
        @Path("apiKey") apiKey: String = ApiConfiguration.KVwukSRVOw(),
        @Query("l") leagueName: String
    ): Response<TeamsDto>
}
