package com.daly.leagues.data.remote.dto

import com.squareup.moshi.Json

data class LeagueDto(
    @Json(name = "strLeague") val strLeague: String? = null
)
