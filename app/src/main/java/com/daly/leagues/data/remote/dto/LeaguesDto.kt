package com.daly.leagues.data.remote.dto

import com.squareup.moshi.Json

data class LeaguesDto(
    @Json(name = "leagues") val leagues: List<LeagueDto>? = null
)
