package com.daly.leagues.data.remote.dto

import com.squareup.moshi.Json

data class TeamDto(
    @Json(name = "strTeam") var strTeam: String? = null,
    @Json(name = "strTeamBadge") var strTeamBadge: String? = null
)
