package com.daly.leagues.data.remote.dto

import com.squareup.moshi.Json

data class TeamsDto(
    @Json(name = "teams") val teams: List<TeamDto>? = null
)
