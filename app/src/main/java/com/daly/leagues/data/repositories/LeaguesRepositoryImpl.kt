package com.daly.leagues.data.repositories

import com.daly.leagues.core.network.ApiResult.Error
import com.daly.leagues.core.network.ApiResult.Exception
import com.daly.leagues.core.network.ApiResult.Success
import com.daly.leagues.core.network.handleApi
import com.daly.leagues.data.mappers.toDomainModel
import com.daly.leagues.data.remote.api.SportsDbApi
import com.daly.leagues.domain.repositories.LeaguesRepository
import javax.inject.Inject

/**
 * [LeaguesRepository] implementation to fetch data from [SportsDbApi]
 * @param sportsDbApi - The API used to fetch data
 */
class LeaguesRepositoryImpl @Inject constructor(
    private val sportsDbApi: SportsDbApi
) : LeaguesRepository {

    /**
     * @see [LeaguesRepository.getAllLeagues]
     */
    override suspend fun getAllLeagues() = when (val result = handleApi { sportsDbApi.getLeagues() }) {
        is Error, is Exception -> null
        is Success -> result.data.leagues?.toDomainModel()
    }

    /**
     * @see [LeaguesRepository.getTeamsByLeague]
     */
    override suspend fun getTeamsByLeague(leagueName: String) = when (val result = handleApi { sportsDbApi.getTeamsByLeague(leagueName = leagueName) }) {
        is Error, is Exception -> null
        is Success -> result.data.teams?.toDomainModel()
    }
}
