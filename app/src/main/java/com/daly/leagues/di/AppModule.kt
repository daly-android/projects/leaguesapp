package com.daly.leagues.di

import com.daly.leagues.core.DispatcherService
import com.daly.leagues.core.implementations.DispatcherServiceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    fun provideDispatcherService(): DispatcherService {
        return DispatcherServiceImpl()
    }
}
