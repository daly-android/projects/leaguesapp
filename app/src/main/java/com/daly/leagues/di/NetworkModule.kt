package com.daly.leagues.di

import android.content.Context
import com.daly.leagues.core.network.HttpCache
import com.daly.leagues.core.network.OkHttpClientBase
import com.daly.leagues.core.network.RetrofitBuilder
import com.daly.leagues.data.remote.api.SportsDbApi
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    @Singleton
    fun provideRetrofitClient(moshi: Moshi, okHttpClient: OkHttpClient): SportsDbApi {
        return RetrofitBuilder(moshi, okHttpClient).build(SportsDbApi::class.java)
    }

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder()
        .addLast(KotlinJsonAdapterFactory())
        .build()

    @Provides
    @Singleton
    fun provideOkHttpClient(httpCache: HttpCache): OkHttpClient = OkHttpClientBase.buildOkHttpClient(httpCache)

    @Provides
    @Singleton
    fun provideHttpCache(@ApplicationContext context: Context): HttpCache = HttpCache(context)
}
