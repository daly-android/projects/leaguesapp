package com.daly.leagues.domain.models

data class BuildLeaguesSuggestionsInput(
    val newText: String,
    val leagues: List<String>,
    val history: List<String>
)
