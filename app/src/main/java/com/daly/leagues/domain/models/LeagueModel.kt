package com.daly.leagues.domain.models

data class LeagueModel(
    val name: String
)
