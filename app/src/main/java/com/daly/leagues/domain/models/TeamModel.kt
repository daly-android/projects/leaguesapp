package com.daly.leagues.domain.models

data class TeamModel(
    val name: String,
    val badgeUrl: String?
)
