package com.daly.leagues.domain.repositories

import com.daly.leagues.domain.models.LeagueModel
import com.daly.leagues.domain.models.TeamModel

/**
 * Repository to fetch leagues and teams related data
 */
interface LeaguesRepository {

    /**
     * Get the list of all leagues
     * @return List<[LeagueModel]>
     */
    suspend fun getAllLeagues(): List<LeagueModel>?

    /**
     * Get the team's list of the given league
     * @param leagueName - The league name to get it's teams list
     * @return List<[TeamModel]>
     */
    suspend fun getTeamsByLeague(leagueName: String): List<TeamModel>?
}
