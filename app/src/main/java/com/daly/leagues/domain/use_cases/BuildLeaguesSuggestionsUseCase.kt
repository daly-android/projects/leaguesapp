package com.daly.leagues.domain.use_cases

import com.daly.leagues.domain.models.BuildLeaguesSuggestionsInput
import javax.inject.Inject

/**
 * Use case to build leagues suggestions using user search input
 * If no suggestion matching user input, an empty list is returned
 */
class BuildLeaguesSuggestionsUseCase @Inject constructor() {

    private companion object {
        const val MIN_LEAGUE_LENGTH_TO_BUILD_SUGGESTIONS = 2
    }

    operator fun invoke(input: BuildLeaguesSuggestionsInput): List<String> = if (input.newText.length >= MIN_LEAGUE_LENGTH_TO_BUILD_SUGGESTIONS) {
        input.leagues
            .filter {
                it.contains(input.newText, ignoreCase = true)
            }
            .filterNot {
                input.history.contains(it)
            }
    } else {
        emptyList()
    }
}
