package com.daly.leagues.domain.use_cases

import com.daly.leagues.domain.repositories.LeaguesRepository
import javax.inject.Inject

/**
 * Use case to get leagues list using any [LeaguesRepository] implementation provided by dependency injection module
 * @param leaguesRepository - Given implementation of [LeaguesRepository]
 */
class GetLeaguesUseCase @Inject constructor(
    private val leaguesRepository: LeaguesRepository
) {
    suspend operator fun invoke() = leaguesRepository.getAllLeagues()
}
