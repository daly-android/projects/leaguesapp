package com.daly.leagues.domain.use_cases

import com.daly.leagues.domain.models.TeamModel
import com.daly.leagues.domain.repositories.LeaguesRepository
import javax.inject.Inject

/**
 * Use case to get teams list for the given league name using any [LeaguesRepository] implementation provided by dependency injection module
 * @param leaguesRepository - Given implementation of [LeaguesRepository]
 */
class GetTeamsForLeagueUseCase @Inject constructor(
    private val leaguesRepository: LeaguesRepository
) {
    suspend operator fun invoke(leagueName: String): List<TeamModel>? = leaguesRepository.getTeamsByLeague(leagueName)?.let { teamsResult ->
        teamsResult.sortedByDescending { it.name }.filterIndexed { index, _ -> (index % 2 == 0) }
    }
}
