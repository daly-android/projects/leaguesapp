package com.daly.leagues.presentation.search_league

sealed interface SearchAction {
    data class OnSearch(val league: String) : SearchAction
    data class OnQueryChange(val newQuery: String) : SearchAction
    data class OnActiveChange(val isActive: Boolean) : SearchAction
    data object OnTrailingClick : SearchAction
}
