package com.daly.leagues.presentation.search_league

data class SearchData(
    val searchedText: String = "",
    val isActive: Boolean = false,
    val suggestions: List<String> = mutableListOf(),
    val history: MutableList<String> = mutableListOf()
)
