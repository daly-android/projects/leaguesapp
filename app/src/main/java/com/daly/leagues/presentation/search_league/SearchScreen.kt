package com.daly.leagues.presentation.search_league

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.daly.leagues.R
import com.daly.leagues.core.theme.LeaguesAppTheme
import com.daly.leagues.presentation.search_league.components.ErrorBox
import com.daly.leagues.presentation.search_league.components.Loader
import com.daly.leagues.presentation.search_league.components.SearchBarView
import com.daly.leagues.presentation.search_league.components.TeamItemView
import kotlin.math.max

@Composable
fun SearchScreenRoot(
    viewModel: SearchViewModel = hiltViewModel()
) {
    val searchState = viewModel.searchState.collectAsStateWithLifecycle()
    val teamsState = viewModel.teamsState.collectAsStateWithLifecycle()

    SearchScree(
        searchData = searchState.value,
        teamsState = teamsState.value,
        onAction = viewModel::onAction
    )
}

@Composable
private fun SearchScree(
    searchData: SearchData,
    teamsState: SearchState,
    onAction: (SearchAction) -> Unit
) {
    Column {
        SearchBarView(
            text = searchData.searchedText,
            active = searchData.isActive,
            suggestions = searchData.suggestions,
            history = searchData.history,
            onQueryChange = { onAction(SearchAction.OnQueryChange(it)) },
            onSearch = { onAction(SearchAction.OnSearch(it)) },
            onActiveChange = { onAction(SearchAction.OnActiveChange(it)) },
            onTrailingIconClick = { onAction(SearchAction.OnTrailingClick) },
            modifier = Modifier
                .padding(16.dp)
                .testTag(stringResource(id = R.string.search_teams_for_league_test_tag))
        )

        when (teamsState) {
            SearchState.Error -> {
                ErrorBox(
                    error = stringResource(id = R.string.teams_error),
                    modifier = Modifier.fillMaxSize()
                )
            }
            is SearchState.Searching -> {
                if (teamsState.inProgress) {
                    Loader()
                }
            }
            is SearchState.Success -> {
                var maxHeight by remember {
                    mutableIntStateOf(0)
                }
                val maxHeightDp = with(LocalDensity.current) {
                    maxHeight.toDp()
                }

                LazyVerticalGrid(
                    modifier = Modifier.testTag(stringResource(id = R.string.teams_list_test_tag)),
                    columns = GridCells.Fixed(2),
                    contentPadding = PaddingValues(6.dp)
                ) {
                    items(
                        items = teamsState.teams,
                        key = { team -> team.name }
                    ) { team ->
                        TeamItemView(
                            team = team,
                            modifier = Modifier
                                .padding(6.dp)
                                .defaultMinSize(minHeight = maxHeightDp)
                                .onSizeChanged { maxHeight = max(maxHeight, it.height) }
                                .testTag(stringResource(id = R.string.team_item_test_tag))
                        )
                    }
                }
            }
        }
    }
}

@Preview
@Composable
private fun SearchScreePreview() {
    LeaguesAppTheme {
        SearchScree(
            searchData = SearchData(),
            teamsState = SearchState.Searching(inProgress = true),
            onAction = {}
        )
    }
}
