package com.daly.leagues.presentation.search_league

import com.daly.leagues.domain.models.TeamModel

sealed interface SearchState {
    data object Error : SearchState
    data class Searching(val inProgress: Boolean = false) : SearchState
    data class Success(val teams: List<TeamModel> = listOf()) : SearchState
}
