package com.daly.leagues.presentation.search_league

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.daly.leagues.core.DispatcherService
import com.daly.leagues.domain.models.BuildLeaguesSuggestionsInput
import com.daly.leagues.domain.use_cases.BuildLeaguesSuggestionsUseCase
import com.daly.leagues.domain.use_cases.GetLeaguesUseCase
import com.daly.leagues.domain.use_cases.GetTeamsForLeagueUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val getLeaguesUseCase: GetLeaguesUseCase,
    private val getTeamsForLeagueUseCase: GetTeamsForLeagueUseCase,
    private val buildLeaguesSuggestionsUseCase: BuildLeaguesSuggestionsUseCase,
    private val dispatcherService: DispatcherService
) : ViewModel() {

    private var _searchState: MutableStateFlow<SearchData> = MutableStateFlow(SearchData())
    val searchState: StateFlow<SearchData> = _searchState.stateIn(
        initialValue = _searchState.value,
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000)
    )

    private val _teamsState: MutableStateFlow<SearchState> = MutableStateFlow(SearchState.Searching())
    val teamsState: StateFlow<SearchState> = _teamsState.stateIn(
        initialValue = _teamsState.value,
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000)
    )

    private var leagues: List<String> = listOf()

    init {
        viewModelScope.launch(dispatcherService.io) {
            getLeaguesUseCase()?.let { leaguesResult ->
                leagues = leaguesResult.map { it.name }
            }
        }
    }

    fun onAction(action: SearchAction) {
        when (action) {
            is SearchAction.OnActiveChange -> onActiveChange(action.isActive)
            is SearchAction.OnQueryChange -> onQueryChange(action.newQuery)
            is SearchAction.OnSearch -> onSearch(action.league)
            SearchAction.OnTrailingClick -> onTrailingIconClick()
        }
    }

    private fun onActiveChange(isActive: Boolean) {
        _searchState.value = _searchState.value.copy(isActive = isActive)
        _teamsState.value = SearchState.Searching(inProgress = false)
    }

    private fun onQueryChange(newQuery: String) {
        _searchState.value = _searchState.value.copy(
            searchedText = newQuery,
            suggestions = buildLeaguesSuggestionsUseCase(
                BuildLeaguesSuggestionsInput(
                    newText = newQuery,
                    leagues = leagues,
                    history = searchState.value.history
                )
            )
        )
        _teamsState.value = SearchState.Searching(inProgress = false)
    }

    private fun onSearch(leagueName: String) {
        _teamsState.value = SearchState.Searching(inProgress = true)
        _searchState.value = _searchState.value.copy(
            searchedText = leagueName,
            isActive = false,
            suggestions = mutableListOf()
        ).apply {
            if (!history.contains(leagueName)) {
                history.add(leagueName)
            }
        }
        getTeams(leagueName)
    }

    private fun getTeams(leagueName: String) {
        viewModelScope.launch(dispatcherService.io) {
            val resultTeams = getTeamsForLeagueUseCase(leagueName)
            resultTeams?.let {
                _teamsState.value = SearchState.Success(teams = resultTeams)
            } ?: run {
                _teamsState.value = SearchState.Error
            }
        }
    }

    private fun onTrailingIconClick() {
        with(_searchState.value) {
            if (isActive) {
                if (searchedText.isNotEmpty()) {
                    _searchState.value = _searchState.value.copy(searchedText = "", suggestions = mutableListOf())
                } else {
                    _searchState.value = _searchState.value.copy(isActive = false)
                }
                _teamsState.value = SearchState.Searching(inProgress = false)
            }
        }
    }
}
