@file:OptIn(ExperimentalMaterial3Api::class)

package com.daly.leagues.presentation.search_league.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.History
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.daly.leagues.R

@Composable
fun SearchBarView(
    text: String,
    active: Boolean,
    suggestions: List<String>,
    history: List<String>,
    onQueryChange: (String) -> Unit,
    onSearch: (String) -> Unit,
    onActiveChange: (Boolean) -> Unit,
    onTrailingIconClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    SearchBar(
        modifier = modifier.fillMaxWidth(),
        query = text,
        onQueryChange = onQueryChange,
        onSearch = onSearch,
        active = active,
        onActiveChange = onActiveChange,
        placeholder = {
            Text(text = stringResource(R.string.search_bar_place_holder))
        },
        leadingIcon = {
            Icon(imageVector = Icons.Default.Search, contentDescription = stringResource(R.string.search_icon_content_description))
        },
        trailingIcon = {
            if (active) {
                Icon(
                    modifier = Modifier.clickable(onClick = onTrailingIconClick),
                    imageVector = Icons.Default.Close,
                    contentDescription = stringResource(R.string.close_icon_content_description)
                )
            }
        }
    ) {
        history.forEach {
            Row(
                modifier = Modifier
                    .padding(all = 14.dp)
                    .clickable { onSearch(it) }
            ) {
                Icon(
                    imageVector = Icons.Default.History,
                    contentDescription = stringResource(R.string.history_icon_content_description)
                )
                Text(text = it)
            }
        }

        suggestions.forEach {
            Row(
                modifier = Modifier
                    .padding(all = 14.dp)
                    .clickable { onSearch(it) }
            ) {
                Text(text = it)
            }
        }
    }
}
