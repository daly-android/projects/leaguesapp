package com.daly.leagues.data.mappers

import com.daly.leagues.data.remote.dto.LeagueDto
import com.daly.leagues.utils.InstantExecutorExtension
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
internal class LeaguesMapperTest {

    private companion object {
        const val LEAGUE_1 = "LEAGUE_1"
        const val LEAGUE_2 = "LEAGUE_2"
    }

    @Test
    fun listLeagueDtoToDomainModel() {
        // given
        val listLeagueDto = listOf(
            LeagueDto(
                strLeague = LEAGUE_1
            ),
            LeagueDto(
                strLeague = LEAGUE_2
            )
        )

        // when
        val result = listLeagueDto.toDomainModel()

        // then
        assertNotNull(result)
        with(result) {
            assertEquals(2, this.size)

            assertEquals(LEAGUE_1, this[0].name)
            assertEquals(LEAGUE_2, this[1].name)
        }
    }
}
