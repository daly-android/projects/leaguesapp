package com.daly.leagues.data.mappers

import com.daly.leagues.data.remote.dto.TeamDto
import com.daly.leagues.utils.InstantExecutorExtension
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
internal class TeamsMapperTest {

    private companion object {
        const val TEAM_NAME_1 = "TEAM_NAME_1"
        const val TEAM_NAME_2 = "TEAM_NAME_2"
        const val TEAM_URL_1 = "TEAM_URL_1"
        const val TEAM_URL_2 = "TEAM_URL_2"
    }

    @Test
    fun listTeamDtoToDomainModel() {
        // given
        val listTeamDto = listOf(
            TeamDto(
                strTeam = TEAM_NAME_1,
                strTeamBadge = TEAM_URL_1
            ),
            TeamDto(
                strTeam = TEAM_NAME_2,
                strTeamBadge = TEAM_URL_2
            )
        )

        // when
        val result = listTeamDto.toDomainModel()

        // then
        assertNotNull(result)
        with(result) {
            assertEquals(2, this.size)

            assertEquals(TEAM_NAME_1, this[0].name)
            assertEquals(TEAM_URL_1, this[0].badgeUrl)

            assertEquals(TEAM_NAME_2, this[1].name)
            assertEquals(TEAM_URL_2, this[1].badgeUrl)
        }
    }
}
