package com.daly.leagues.data.repositories

import com.daly.leagues.data.remote.api.SportsDbApi
import com.daly.leagues.data.remote.dto.LeagueDto
import com.daly.leagues.data.remote.dto.LeaguesDto
import com.daly.leagues.data.remote.dto.TeamDto
import com.daly.leagues.data.remote.dto.TeamsDto
import com.daly.leagues.utils.InstantExecutorExtension
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import retrofit2.Response

@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
internal class LeaguesRepositoryImplTest {

    private companion object {
        const val LEAGUE_1 = "LEAGUE_1"
        const val LEAGUE_2 = "LEAGUE_2"

        const val TEAM_NAME_1 = "TEAM_NAME_1"
        const val TEAM_NAME_2 = "TEAM_NAME_2"
        const val TEAM_URL_1 = "TEAM_URL_1"
        const val TEAM_URL_2 = "TEAM_URL_2"

        val leaguesListApiResult = LeaguesDto(
            leagues = listOf(
                LeagueDto(
                    strLeague = LEAGUE_1
                ),
                LeagueDto(
                    strLeague = LEAGUE_2
                )
            )
        )

        val teamsListApiResult = TeamsDto(
            teams = listOf(
                TeamDto(
                    strTeam = TEAM_NAME_1,
                    strTeamBadge = TEAM_URL_1
                ),
                TeamDto(
                    strTeam = TEAM_NAME_2,
                    strTeamBadge = TEAM_URL_2
                )
            )
        )
    }

    @MockK
    private lateinit var sportsDbApi: SportsDbApi

    private lateinit var repository: LeaguesRepositoryImpl

    @BeforeEach
    fun setup() {
        repository = LeaguesRepositoryImpl(sportsDbApi = sportsDbApi)
    }

    @Test
    fun testGetLeaguesListSuccess() = runTest {
        // given
        coEvery { sportsDbApi.getLeagues() } answers { Response.success(leaguesListApiResult) }

        // when
        val result = repository.getAllLeagues()

        // then
        coVerify(exactly = 1) {
            sportsDbApi.getLeagues()
        }
        assertNotNull(result)
        with(result!!) {
            assertEquals(2, this.size)

            assertEquals(LEAGUE_1, this[0].name)
            assertEquals(LEAGUE_2, this[1].name)
        }
    }

    @Test
    fun testGetLeaguesListFailure() = runTest {
        // given
        coEvery { sportsDbApi.getLeagues() } answers { Response.error(400, null) }

        // when
        val result = repository.getAllLeagues()

        // then
        assertNull(result)
        coVerify(exactly = 1) {
            sportsDbApi.getLeagues()
        }
    }

    @Test
    fun testGetLeaguesListError() = runTest {
        // given
        coEvery { sportsDbApi.getLeagues() } answers { throw RuntimeException() }

        // when
        val result = repository.getAllLeagues()

        // then
        assertNull(result)
        coVerify(exactly = 1) {
            sportsDbApi.getLeagues()
        }
    }

    @Test
    fun testGetTeamsByLeagueSuccess() = runTest {
        // given
        coEvery { sportsDbApi.getTeamsByLeague(leagueName = LEAGUE_1) } answers { Response.success(teamsListApiResult) }

        // when
        val result = repository.getTeamsByLeague(leagueName = LEAGUE_1)

        // then
        coVerify(exactly = 1) {
            sportsDbApi.getTeamsByLeague(leagueName = LEAGUE_1)
        }
        assertNotNull(result)
        with(result!!) {
            assertEquals(2, this.size)

            assertEquals(TEAM_NAME_1, this[0].name)
            assertEquals(TEAM_URL_1, this[0].badgeUrl)

            assertEquals(TEAM_NAME_2, this[1].name)
            assertEquals(TEAM_URL_2, this[1].badgeUrl)
        }
    }

    @Test
    fun testGetTeamsByLeagueFailure() = runTest {
        // given
        coEvery { sportsDbApi.getTeamsByLeague(leagueName = LEAGUE_1) } answers { Response.error(400, null) }

        // when
        val result = repository.getTeamsByLeague(leagueName = LEAGUE_1)

        // then
        assertNull(result)
        coVerify(exactly = 1) {
            sportsDbApi.getTeamsByLeague(leagueName = LEAGUE_1)
        }
    }

    @Test
    fun testGetTeamsByLeagueError() = runTest {
        // given
        coEvery { sportsDbApi.getTeamsByLeague(leagueName = LEAGUE_1) } answers { throw RuntimeException() }

        // when
        val result = repository.getTeamsByLeague(leagueName = LEAGUE_1)

        // then
        assertNull(result)
        coVerify(exactly = 1) {
            sportsDbApi.getTeamsByLeague(leagueName = LEAGUE_1)
        }
    }
}
