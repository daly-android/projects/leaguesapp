package com.daly.leagues.domain.use_cases

import com.daly.leagues.domain.models.BuildLeaguesSuggestionsInput
import com.daly.leagues.utils.InstantExecutorExtension
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
internal class BuildLeaguesSuggestionsUseCaseTest {

    private companion object {
        const val LEAGUE_1 = "abcd"
        const val LEAGUE_2 = "efgh"
        const val LEAGUE_3 = "ijkl"

        val leagues = listOf(LEAGUE_1, LEAGUE_2, LEAGUE_3)

        val history = listOf(LEAGUE_3)

        @JvmStatic
        fun suggestionsInputArguments(): Stream<Arguments> = Stream.of(
            Arguments.of(
                BuildLeaguesSuggestionsInput(
                    newText = "",
                    leagues = leagues,
                    history = history
                ),
                emptyList<String>()
            ),
            Arguments.of(
                BuildLeaguesSuggestionsInput(
                    newText = "a",
                    leagues = leagues,
                    history = history
                ),
                emptyList<String>()
            ),
            Arguments.of(
                BuildLeaguesSuggestionsInput(
                    newText = LEAGUE_1,
                    leagues = emptyList(),
                    history = history
                ),
                emptyList<String>()
            ),
            Arguments.of(
                BuildLeaguesSuggestionsInput(
                    newText = "mpol",
                    leagues = leagues,
                    history = history
                ),
                emptyList<String>()
            ),
            Arguments.of(
                BuildLeaguesSuggestionsInput(
                    newText = LEAGUE_3,
                    leagues = leagues,
                    history = history
                ),
                emptyList<String>()
            ),
            Arguments.of(
                BuildLeaguesSuggestionsInput(
                    newText = LEAGUE_1,
                    leagues = leagues,
                    history = history
                ),
                listOf(LEAGUE_1)
            ),
            Arguments.of(
                BuildLeaguesSuggestionsInput(
                    newText = "ab",
                    leagues = leagues,
                    history = history
                ),
                listOf(LEAGUE_1)
            ),
            Arguments.of(
                BuildLeaguesSuggestionsInput(
                    newText = "AbC",
                    leagues = leagues,
                    history = history
                ),
                listOf(LEAGUE_1)
            )
        )
    }

    private lateinit var useCase: BuildLeaguesSuggestionsUseCase

    @BeforeEach
    fun setup() {
        useCase = BuildLeaguesSuggestionsUseCase()
    }

    @ParameterizedTest
    @MethodSource("suggestionsInputArguments")
    fun testInvoke(input: BuildLeaguesSuggestionsInput, expectedResult: List<String>) {
        // when
        val result = useCase.invoke(input)

        // then
        assertNotNull(result)
        assertEquals(expectedResult, result)
    }
}
