package com.daly.leagues.domain.use_cases

import com.daly.leagues.domain.models.LeagueModel
import com.daly.leagues.domain.repositories.LeaguesRepository
import com.daly.leagues.utils.InstantExecutorExtension
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
internal class GetLeaguesUseCaseTest {

    private companion object {
        const val LEAGUE_1 = "LEAGUE_1"
        const val LEAGUE_2 = "LEAGUE_2"
    }

    @MockK
    private lateinit var leaguesRepository: LeaguesRepository

    private lateinit var useCase: GetLeaguesUseCase

    @BeforeEach
    fun setup() {
        useCase = GetLeaguesUseCase(leaguesRepository = leaguesRepository)
    }

    @Test
    fun testInvokeSuccess() = runTest {
        // given
        val givenListOfLeagueModel = listOf(
            LeagueModel(name = LEAGUE_1),
            LeagueModel(name = LEAGUE_2)
        )
        coEvery { leaguesRepository.getAllLeagues() } returns givenListOfLeagueModel

        // when
        val result = useCase.invoke()

        // then
        assertNotNull(result)
        with(result!!) {
            assertEquals(2, this.size)

            assertEquals(LEAGUE_1, this[0].name)
            assertEquals(LEAGUE_2, this[1].name)
        }
        coVerify(exactly = 1) {
            leaguesRepository.getAllLeagues()
        }
    }

    @Test
    fun testInvokeFailure() = runTest {
        // given
        coEvery { leaguesRepository.getAllLeagues() } returns null

        // when
        val result = useCase.invoke()

        // then
        assertNull(result)
        coVerify(exactly = 1) {
            leaguesRepository.getAllLeagues()
        }
    }
}
