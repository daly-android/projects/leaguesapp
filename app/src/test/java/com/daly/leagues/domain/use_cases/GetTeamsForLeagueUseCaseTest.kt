package com.daly.leagues.domain.use_cases

import com.daly.leagues.domain.models.TeamModel
import com.daly.leagues.domain.repositories.LeaguesRepository
import com.daly.leagues.utils.InstantExecutorExtension
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
internal class GetTeamsForLeagueUseCaseTest {

    private companion object {
        const val LEAGUE_NAME = "LEAGUE_NAME"

        const val TEAM_NAME_1 = "TEAM_NAME_1"
        const val TEAM_NAME_2 = "TEAM_NAME_2"
        const val TEAM_NAME_3 = "TEAM_NAME_3"
        const val TEAM_URL_1 = "TEAM_URL_1"
        const val TEAM_URL_2 = "TEAM_URL_2"
        const val TEAM_URL_3 = "TEAM_URL_3"
    }

    @MockK
    private lateinit var leaguesRepository: LeaguesRepository

    private lateinit var useCase: GetTeamsForLeagueUseCase

    @BeforeEach
    fun setup() {
        useCase = GetTeamsForLeagueUseCase(leaguesRepository = leaguesRepository)
    }

    @Test
    fun testInvokeSuccess() = runTest {
        // given
        val givenListOfTeamModel = listOf(
            TeamModel(
                name = TEAM_NAME_1,
                badgeUrl = TEAM_URL_1
            ),
            TeamModel(
                name = TEAM_NAME_2,
                badgeUrl = TEAM_URL_2
            ),
            TeamModel(
                name = TEAM_NAME_3,
                badgeUrl = TEAM_URL_3
            )
        )
        coEvery { leaguesRepository.getTeamsByLeague(leagueName = LEAGUE_NAME) } returns givenListOfTeamModel

        // when
        val result = useCase.invoke(leagueName = LEAGUE_NAME)

        // then
        assertNotNull(result)
        with(result!!) {
            assertEquals(2, this.size)

            assertEquals(TEAM_NAME_3, this[0].name)
            assertEquals(TEAM_URL_3, this[0].badgeUrl)

            assertEquals(TEAM_NAME_1, this[1].name)
            assertEquals(TEAM_URL_1, this[1].badgeUrl)
        }
        coVerify(exactly = 1) {
            leaguesRepository.getTeamsByLeague(leagueName = LEAGUE_NAME)
        }
    }

    @Test
    fun testInvokeSuccessOneItemResultList() = runTest {
        // given
        val givenListOfTeamModel = listOf(
            TeamModel(
                name = TEAM_NAME_1,
                badgeUrl = TEAM_URL_1
            )
        )
        coEvery { leaguesRepository.getTeamsByLeague(leagueName = LEAGUE_NAME) } returns givenListOfTeamModel

        // when
        val result = useCase.invoke(leagueName = LEAGUE_NAME)

        // then
        assertNotNull(result)
        with(result!!) {
            assertEquals(1, this.size)

            assertEquals(TEAM_NAME_1, this[0].name)
            assertEquals(TEAM_URL_1, this[0].badgeUrl)
        }
        coVerify(exactly = 1) {
            leaguesRepository.getTeamsByLeague(leagueName = LEAGUE_NAME)
        }
    }

    @Test
    fun testInvokeSuccessEmptyResultList() = runTest {
        // given
        val givenListOfTeamModel = listOf<TeamModel>()
        coEvery { leaguesRepository.getTeamsByLeague(leagueName = LEAGUE_NAME) } returns givenListOfTeamModel

        // when
        val result = useCase.invoke(leagueName = LEAGUE_NAME)

        // then
        assertNotNull(result)
        assertTrue(result!!.isEmpty())
        coVerify(exactly = 1) {
            leaguesRepository.getTeamsByLeague(leagueName = LEAGUE_NAME)
        }
    }

    @Test
    fun testInvokeFailure() = runTest {
        // given
        coEvery { leaguesRepository.getTeamsByLeague(leagueName = LEAGUE_NAME) } returns null

        // when
        val result = useCase.invoke(leagueName = LEAGUE_NAME)

        // then
        assertNull(result)
        coVerify(exactly = 1) {
            leaguesRepository.getTeamsByLeague(leagueName = LEAGUE_NAME)
        }
    }
}
