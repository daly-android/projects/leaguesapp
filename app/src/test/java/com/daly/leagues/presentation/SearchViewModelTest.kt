package com.daly.leagues.presentation

import app.cash.turbine.test
import com.daly.leagues.core.DispatcherService
import com.daly.leagues.domain.models.LeagueModel
import com.daly.leagues.domain.models.TeamModel
import com.daly.leagues.domain.use_cases.BuildLeaguesSuggestionsUseCase
import com.daly.leagues.domain.use_cases.GetLeaguesUseCase
import com.daly.leagues.domain.use_cases.GetTeamsForLeagueUseCase
import com.daly.leagues.presentation.search_league.SearchAction
import com.daly.leagues.presentation.search_league.SearchState
import com.daly.leagues.presentation.search_league.SearchViewModel
import com.daly.leagues.utils.InstantExecutorExtension
import com.daly.leagues.utils.TestDispatcherService
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
internal class SearchViewModelTest {

    private companion object {
        const val LEAGUE_1 = "abcd"
        const val LEAGUE_2 = "efgh"
        const val LEAGUE_3 = "ijkl"

        const val TEAM_NAME_1 = "TEAM_NAME_1"
        const val TEAM_NAME_2 = "TEAM_NAME_2"
        const val TEAM_NAME_3 = "TEAM_NAME_3"
        const val TEAM_URL_1 = "TEAM_URL_1"
        const val TEAM_URL_2 = "TEAM_URL_2"
        const val TEAM_URL_3 = "TEAM_URL_3"

        val givenListOfLeagueModel = listOf(
            LeagueModel(name = LEAGUE_1),
            LeagueModel(name = LEAGUE_2),
            LeagueModel(name = LEAGUE_3)
        )

        val givenListOfTeamModel = listOf(
            TeamModel(
                name = TEAM_NAME_1,
                badgeUrl = TEAM_URL_1
            ),
            TeamModel(
                name = TEAM_NAME_2,
                badgeUrl = TEAM_URL_2
            ),
            TeamModel(
                name = TEAM_NAME_3,
                badgeUrl = TEAM_URL_3
            )
        )
    }

    @MockK
    private lateinit var getLeaguesUseCase: GetLeaguesUseCase

    @MockK
    private lateinit var getTeamsForLeagueUseCase: GetTeamsForLeagueUseCase

    @MockK
    private lateinit var buildLeaguesSuggestionsUseCase: BuildLeaguesSuggestionsUseCase

    private val dispatcherService: DispatcherService = TestDispatcherService()

    private lateinit var viewModel: SearchViewModel

    @BeforeEach
    fun setup() {
        defaultMocks()
        viewModel = SearchViewModel(
            getLeaguesUseCase = getLeaguesUseCase,
            getTeamsForLeagueUseCase = getTeamsForLeagueUseCase,
            buildLeaguesSuggestionsUseCase = buildLeaguesSuggestionsUseCase,
            dispatcherService = dispatcherService
        )
    }

    @Test
    fun testInitLeagues() = runTest {
        coVerify(exactly = 1) {
            getLeaguesUseCase()
        }
    }

    @Test
    fun onActionOnQueryChange() = runTest {
        // given
        val newQuery = "abc"
        val suggestions = listOf("abcd")
        coEvery { buildLeaguesSuggestionsUseCase(any()) } returns suggestions

        // when
        viewModel.onAction(SearchAction.OnQueryChange(newQuery = newQuery))

        // then
        viewModel.searchState.test {
            skipItems(1) // Skip the initial value

            val awaitingResult = awaitItem()
            assertEquals(newQuery, awaitingResult.searchedText)
            assertEquals(suggestions, awaitingResult.suggestions)

            cancelAndIgnoreRemainingEvents()
        }

        viewModel.teamsState.test {
            val awaitingResult = awaitItem()

            assertTrue(awaitingResult is SearchState.Searching)
            assertFalse((awaitingResult as SearchState.Searching).inProgress)
            cancelAndIgnoreRemainingEvents()
        }
    }

    @ParameterizedTest
    @ValueSource(booleans = [true, false])
    fun onActionOnActiveChange(isActive: Boolean) = runTest {
        // when
        viewModel.onAction(SearchAction.OnActiveChange(isActive = isActive))

        // then
        viewModel.searchState.test {
            if (isActive) {
                skipItems(1) // Skip the initial value
            }

            val awaitingResult = awaitItem()
            assertEquals(isActive, awaitingResult.isActive)

            cancelAndIgnoreRemainingEvents()
        }

        viewModel.teamsState.test {
            val awaitingResult = awaitItem()

            assertTrue(awaitingResult is SearchState.Searching)
            assertFalse((awaitingResult as SearchState.Searching).inProgress)
            cancelAndIgnoreRemainingEvents()
        }
    }

    @ParameterizedTest
    @ValueSource(booleans = [true, false])
    fun onActionOnSearch(getTeamsOnFailure: Boolean) = runTest {
        // given
        val leagueName = "abc"
        val suggestions = listOf("abcd")
        coEvery { buildLeaguesSuggestionsUseCase(any()) } returns suggestions
        if (getTeamsOnFailure) {
            coEvery { getTeamsForLeagueUseCase(any()) } returns null
        }

        // when
        viewModel.onAction(SearchAction.OnSearch(league = leagueName))

        // then
        viewModel.teamsState.test {
            skipItems(1) // Skip the initial value

            if (getTeamsOnFailure) {
                assertTrue(awaitItem() is SearchState.Error)
            } else {
                assertTrue(awaitItem() is SearchState.Success)
            }
            cancelAndIgnoreRemainingEvents()
        }

        viewModel.searchState.test {
            skipItems(1) // Skip the initial value

            val awaitingResult = awaitItem()
            assertFalse(awaitingResult.isActive)
            assertEquals(leagueName, awaitingResult.searchedText)
            assertEquals(emptyList<String>(), awaitingResult.suggestions)

            cancelAndIgnoreRemainingEvents()
        }

        coVerify(exactly = 1) {
            getTeamsForLeagueUseCase(leagueName)
        }
    }

    @Test
    fun onActionOnTrailingIconClick() = runTest {
        // when
        viewModel.onAction(SearchAction.OnTrailingClick)

        // then
        with(viewModel) {
            assertEquals("", searchState.value.searchedText)
            assertEquals(emptyList<String>(), searchState.value.suggestions)

            assertTrue(teamsState.value is SearchState.Searching)
            assertFalse((teamsState.value as SearchState.Searching).inProgress)
        }
    }

    private fun defaultMocks() {
        coEvery { getLeaguesUseCase.invoke() } returns givenListOfLeagueModel
        coEvery { getTeamsForLeagueUseCase.invoke(any()) } returns givenListOfTeamModel
        coEvery { buildLeaguesSuggestionsUseCase.invoke(any()) } returns emptyList()
    }
}
