package com.daly.leagues.utils

import com.daly.leagues.core.DispatcherService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlin.coroutines.CoroutineContext

/** Test dispatcher service link only to Main dispatcher. */
@OptIn(ExperimentalCoroutinesApi::class)
class TestDispatcherService : DispatcherService {

    private val testDispatcher = UnconfinedTestDispatcher()

    override val io: CoroutineContext get() = testDispatcher
    override val main: CoroutineContext get() = testDispatcher
    override val default: CoroutineContext get() = testDispatcher
    override val unconfined: CoroutineContext get() = testDispatcher
}
